<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Data Bagian</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body background="<?php echo base_url(); ?>assets/image/background-2.jpg">

<!-- <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#"></a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href=""></a></li>
      <li class="active"><a href=""></a></li>
    </ul>
  </div>
</nav> -->

<div class="w3-sidebar w3-dark-grey w3-bar-block" style="width: 15%">
  <a href="" class="w3-bar-item" style="text-decoration: none;background-color:  #2C6AA0;"><h3><b>COBA_CI</b></h3></a>
  <a href="<?php echo base_url(); ?>index.php/c_karyawan" class="w3-bar-item w3-button" style="text-decoration: none;">
  <span class="glyphicon glyphicon-user"></span>	Karyawan</a>
  <a href="<?php echo base_url(); ?>index.php/c_bagian" class="w3-bar-item w3-button" style="text-decoration: none;">
  <span class="glyphicon glyphicon-briefcase"></span>	Bagian</a>
</div>

<div style="margin-left:16%">

<div class="w3-container" style="background-color:  #2C6AA0;">
  <h4 style="color: white;">DATA BAGIAN</h4>
</div>
<div class="w3-container w3-light-grey" style="border-radius: 0px 0px 5px 5px;">
  <h6><span class="glyphicon glyphicon-home"> </span> Home > <a href="<?php echo base_url(); ?>index.php/c_bagian">Bagian</a></h6>
</div>
<br>
<div class="container w3-light-grey" style="width: 97%;border-radius: 5px 5px 5px 5px;">

<!-- 	<h3 class="page-header text-center">Data Bagian</h3> -->
	<div class="row">
		<div class="col-sm-11 col-sm-offset-0"><br>
			<button type="button" class="btn btn-success btn" data-toggle="modal" data-target="#modalbagian"><span class="glyphicon glyphicon-plus"></span> Tambah Bagian</button>
			<br><br>

			<table id="tabeldata" class="table table-striped table-bordered" width="100%" cellspacing="0"">
				<thead>
					<tr>
						<th class="col-sm-1">ID</th>
						<th>Nama Bagian</th>
						<th class="col-sm-3	">Action</th>
					</tr>
				</thead>
				<tbody>
					
					<?php
					foreach($bagian as $user){
						?>
						<tr>
							<td><?php echo $user->id_bagian; ?></td>
							<td><?php echo $user->nama_bagian; ?></td>
							<td align="center">

							<a style="cursor: pointer; margin-right: 7px;" id="btneditbagian" class="btn-sm btn-primary" data-toggle="modal" data-target="#edit<?php echo $user->id_bagian; ?>"> <span class="glyphicon glyphicon-edit"> </span>
							</a>

							<a style="cursor: pointer; margin-right: 5px;" id="btnhapusbagian" class="btn-sm btn-danger" data-toggle="modal" data-target="#hapus<?php echo $user->id_bagian; ?>"> <span class="glyphicon glyphicon-trash"> </span>
							</a>

							<button id="<?php echo $user->id_bagian; ?>" class="btn btn-default"><span class="glyphicon glyphicon-plus"> </span></button>

							</td>
						</tr>

						<script>
		                  $(document).ready(function() {
		                         
		                    $("#<?php echo $user->id_bagian; ?>").click(function(){
		                        
		                        //  $.ajax({
		                           
		                        //    url : "<?php echo base_url(); ?>index.php/Mbarang/savedata", 
		                        //    type: "POST", 
		                        //    data: $("#form_barang").serialize(),
		                        //      success: function(data) {
		                            
		                        //     $('#list_barang').html(data);
		                        //     }
		                        //  });
		                        // return false;
		                        $('#list_anggota').load("<?php echo base_url();?>index.php/c_karyawan/getdata/<?php echo $user->id_bagian; ?>");

		                          $('#tesajax').show();
		                          $('html, body').animate({
		                          	scrollTop: $("#tesajax").offset().top
    								}, 250);
		                       
		                    }); 
		                    //**end**

		                    //untuk load list barang di awal
		                    //$('#list_barang').load("<?php echo base_url();?>index.php/Mbarang/list_barang");
		                    //**end**
		                    $("#tutupdetail").click(function() {
		                    	$('#tesajax').hide();
		                    });

		                  });

						</script>

						<!-- Modal edit -->
						<div class="container">

						  <div class="modal fade" id="edit<?php echo $user->id_bagian; ?>" role="dialog">
						    <div class="modal-dialog">

						      <div class="modal-content">
						        <div class="modal-header" style="background-color: lightgrey">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">Edit Bagian</h4>
						        </div>
						        <div class="modal-body">
						          <form method="POST" action="<?php echo base_url(); ?>index.php/c_bagian/edit/<?php echo $user->id_bagian; ?>" >
										<div class="form-group">
											<label>Nama Bagian</label> <br>
											<input class="form-control" type="text" name="namabagian" value="<?php echo $user->nama_bagian;?>" >
										</div>
										<button type="submit" class="btn btn-primary btn" ><span class="glyphicon glyphicon-floppy-disk"> Simpan</span></button>
									</form>
						        </div>
						        <div class="modal-footer" style="background-color: lightgrey">
						          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						        </div>
						      </div>
						      
						    </div>
						  </div>
  
						</div>

						<!-- Modal delete -->
						<div class="container">

						  <div class="modal fade" id="hapus<?php echo $user->id_bagian; ?>" role="dialog">
						    <div class="modal-dialog">

						      <div class="modal-content">
						        <div class="modal-header" style="background-color: lightgrey">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">Hapus Bagian</h4>
						        </div>
						        <div class="modal-body">
						          <form method="POST" action="<?php echo base_url(); ?>index.php/c_bagian/delete/<?php echo $user->id_bagian; ?>/<?php echo $user->nama_bagian; ?>" >
										<div class="form-group">
											<label>Nama Bagian</label> <br>
											<input class="form-control" type="text" value="<?php echo $user->nama_bagian; ?>" readonly>
										</div>
										<button type="submit" class="btn btn-danger btn" ><span class="glyphicon glyphicon-trash"> Hapus</span></button>
									</form>
						        </div>
						        <div class="modal-footer" style="background-color: lightgrey">
						          <button type="button" class="btn btn-default btn" data-dismiss="modal">Batal</button>
						        </div>
						      </div>
						      
						    </div>
						  </div>
  
						</div>

						<?php
					}
					?>
				</tbody>
			</table> <br>

		</div>
	</div>
</div>

<br>

<div class="container">

  <div class="modal fade" id="modalbagian" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content w3-light-grey">
        <div class="modal-header" style="background-color: lightgrey">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah Bagian</h4>
        </div>
        <div class="modal-body">
          <form method="POST" action="<?php echo base_url(); ?>index.php/c_bagian/insert">
				<div class="form-group">
					<label>Nama Bagian</label> <br>
					<input class="form-control" type="text" name="nama_bagian" placeholder="Masukan Nama Bagian" required>
				</div>
				<button type="submit" class="btn btn-primary btn" ><span class="glyphicon glyphicon-floppy-disk"> Simpan</span></button>
			</form>
        </div>
        <div class="modal-footer" style="background-color: lightgrey">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

<div id="tesajax" class="container w3-light-grey" style="width: 97%;border-radius: 5px 5px 5px 5px;" hidden>

	<h3 class="page-header text-center">Anggota Bagian</h3>
	<div class="row">
		<div class="col-sm-11 col-sm-offset-0">
		<button id="tutupdetail" type="button" class="btn btn-danger" style="float: right;">Tutup</button>
		<br><br>
			<table id="tabeldata2" class="table table-striped table-bordered" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th class="col-sm-2">ID</th>
						<th>Nama</th>
						<th>Alamat</th>
					</tr>
				</thead>
				<tbody id="list_anggota">
				
				</tbody>
			</table>
		</div>
	</div> <br>
</div>
</div>
<br>
    <script src="<?php echo base_url(); ?>js/jquery-3.1.0.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.dataTables.min.js"></script>
</body>
<br>
<script>
    $(document).ready(function() {
        $('#tabeldata').DataTable();
        $('#tabeldata2').DataTable();
    });
</script>
</html>