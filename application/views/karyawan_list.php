<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Data Karyawan</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body background="<?php echo base_url(); ?>assets/image/background-2.jpg">

<!-- <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Coba_ci</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="<?php echo base_url(); ?>index.php/c_karyawan">Karyawan</a></li>
      <li><a href="<?php echo base_url(); ?>index.php/c_bagian">Bagian</a></li>
    </ul>
  </div>
</nav> -->

<div class="w3-sidebar w3-dark-grey w3-bar-block" style="width:15%">
  <a href="" class="w3-bar-item" style="text-decoration: none;background-color: #2C6AA0"><h5><b>COBA_CI</b></h5></a>
  <a href="<?php echo base_url(); ?>index.php/c_karyawan" class="w3-bar-item w3-button" style="text-decoration: none;">
  <span class="glyphicon glyphicon-user"></span>	Karyawan</a>
  <a href="<?php echo base_url(); ?>index.php/c_bagian" class="w3-bar-item w3-button" style="text-decoration: none;">
  <span class="glyphicon glyphicon-briefcase"></span>	Bagian</a>
</div>

<div style="margin-left:16%">

<div class="w3-container" style="background-color:  #2C6AA0;">
  <h5 style="color: white;">DATA KARYAWAN</h5>
</div>
<div class="w3-container w3-light-grey" style="border-radius: 0px 0px 5px 5px;">
  <p><span class="glyphicon glyphicon-home"> </span> Home > <a href="<?php echo base_url(); ?>index.php/c_karyawan">Karyawan</a></p>
</div>
<br>
<div class="container w3-light-grey" style="width: 97%;border-radius: 5px 5px 5px 5px;">

	<!-- <h3 class="page-header text-center">Data Karyawan</h3> -->
	<div class="row">
		<div class="col-sm-11 col-sm-offset-0"><br>
			<button type="button" class="btn btn-success btn" data-toggle="modal" data-target="#modalkaryawan"><span class="glyphicon glyphicon-plus"></span> Tambah Karyawan</button>
			<br><br>

            // table karyawan
			<table style="text-align: center;" id="tabeldata" class="table table-striped table-bordered" width="100%" cellspacing="0"">
				<thead>
					<tr>
						<th class="col-sm-1">ID</th>
						<th>Nama</th>
						<th>Alamat</th>
						<th class="col-sm-1">ID Bagian</th>
						<th class="col-sm-2">Action</th>
					</tr>
				</thead>
				<tbody>
					
					<?php
					foreach($karyawan as $user){
						?>
						<tr>
							<td><?php echo $user->id_karyawan; ?></td>
							<td><?php echo $user->nama_karyawan; ?></td>
							<td><?php echo $user->alamat; ?></td>
							<td><?php echo $user->id_bagian; ?></td>
							<td align="center">

							<a style="cursor: pointer; margin-right: 5px;" id="btneditkaryawan" class="btn-sm btn-primary" data-toggle="modal" data-target="#edit<?php echo $user->id_karyawan; ?>"> <span class="glyphicon glyphicon-edit"> </span>
							</a>

							<a style="cursor: pointer;" id="btnhapuskaryawan" class="btn-sm btn-danger" data-toggle="modal" data-target="#hapus<?php echo $user->id_karyawan; ?>"> <span class="glyphicon glyphicon-trash"> </span>
							</a>

							</td>
						</tr>

						<!-- Modal edit -->
						<div class="container">

						  <div class="modal fade" id="edit<?php echo $user->id_karyawan; ?>" role="dialog">
						    <div class="modal-dialog">

						      <div class="modal-content">
						        <div class="modal-header" style="background-color: lightgrey">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">Edit Karyawan</h4>
						        </div>
						        <div class="modal-body">
						          <form method="POST" action="<?php echo base_url(); ?>index.php/c_karyawan/edit/<?php echo $user->id_karyawan; ?>" >
										<div class="form-group">
											<label>Nama Karyawan</label> <br>
											<input class="form-control" type="text" name="namakaryawan" value="<?php echo $user->nama_karyawan;?>" >
										</div>
										<div class="form-group">
											<label>Alamat</label> <br>
											<input class="form-control" type="text" name="alamat" value="<?php echo $user->alamat;?>" >
										</div>
										<div class="form-group">
											<label>Bagian</label> <br>
											<select class="select_3" name="idbagian" required>
												<option value="" readonly> Pilih Bagian </option>
												<?php
												foreach($bagian as $row){
												?>
													<option value="<?php echo $row->id_bagian; ?>">
													[<?php echo $row->id_bagian; ?>] 
													<?php echo $row->nama_bagian; ?> </option >
												<?php
												}
												?>
											</select>
										</div>
										<p>Current ID BAGIAN : <?php echo $user->id_bagian; ?></p>
										<button type="submit" class="btn btn-primary btn" ><span class="glyphicon glyphicon-floppy-disk"> Simpan</span></button>
									</form>
						        </div>
						        <div class="modal-footer" style="background-color: lightgrey">
						          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						        </div>
						      </div>
						      
						    </div>
						  </div>
  
						</div>

						<!-- Modal delete -->
						<div class="container">

						  <div class="modal fade" id="hapus<?php echo $user->id_karyawan; ?>" role="dialog">
						    <div class="modal-dialog">

						      <div class="modal-content">
						        <div class="modal-header" style="background-color: lightgrey">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">Hapus Karyawan</h4>
						        </div>
						        <div class="modal-body">
						          <form method="POST" action="<?php echo base_url(); ?>index.php/c_karyawan/delete/<?php echo $user->id_karyawan; ?>" >
										<div class="form-group">
											<label>Nama Karyawan</label> <br>
											<input class="form-control" type="text" value="<?php echo $user->nama_karyawan; ?>" readonly>
										</div>
										<button type="submit" class="btn btn-danger btn" ><span class="glyphicon glyphicon-trash"> Hapus</span></button>
									</form>
						        </div>
						        <div class="modal-footer" style="background-color: lightgrey">
						          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						        </div>
						      </div>
						      
						    </div>
						  </div>
  
						</div>
						<?php
					}
					?>
				</tbody>
			</table> <br>

		</div>
	</div>
</div>
</div>

<div class="container">

  <div class="modal fade" id="modalkaryawan" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header" style="background-color: lightgrey">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah Karyawan</h4>
        </div>
        <div class="modal-body">
          <form method="POST" action="<?php echo base_url(); ?>index.php/c_karyawan/insert">
				<div class="form-group">
					<label>Nama</label> <br>
					<input class="form-control" type="text" name="namakaryawan" placeholder="Masukan Nama" required>
				</div>
				<div class="form-group">
					<label>Alamat</label><br>
					<input class="form-control" type="text" name="alamat" placeholder="Masukan Alamat" required>
				</div>
				<div class="form-group">
					<label>Bagian</label> <br>
							<select class="select_2" name="idbagian" required>
								<option> Pilih Bagian </option>
								<?php
								foreach($bagian as $row){
								?>
		  							<option value="<?php echo $row->id_bagian; ?>"> <?php echo $row->nama_bagian; ?> </option>
							
								<?php
								}
								?>
							</select>
				</div>
				<button type="submit" class="btn btn-primary btn" ><span class="glyphicon glyphicon-floppy-disk"> Simpan</span></button>
			</form>
        </div>
        <div class="modal-footer" style="background-color: lightgrey">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<br><br>
    <script src="<?php echo base_url(); ?>js/jquery-3.1.0.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
</body>

<script>
    $(document).ready(function() {
        $('#tabeldata').DataTable();
        $('.select_2').select2();
        $('.select_3').select2();
    });
</script>
</html>