<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_karyawan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('karyawan_model');
		$this->load->model('bagian_model');
	}

	public function index(){
		$data['karyawan'] = $this->karyawan_model->getAllKaryawan();
		$data['bagian'] = $this->bagian_model->getAllBagian();
		$this->load->view('karyawan_list.php', $data);
	}

	public function insert(){
		$namakaryawan = $this->input->post('namakaryawan');
		$alamat = $this->input->post('alamat');
		$idbagian = $this->input->post('idbagian');
		
		$this->karyawan_model->insertkaryawan($namakaryawan, $alamat, $idbagian);
		
		header('location:'.base_url('index.php/c_karyawan').$this->index()); 
	}

	public function delete($id_karyawan){
		$query = $this->karyawan_model->deletekaryawan($id_karyawan);

			header('location:'.base_url('index.php/c_karyawan').$this->index());
	}

	public function edit($id_karyawan){
		$namakaryawan = $this->input->post('namakaryawan');
		$alamat = $this->input->post('alamat');
		$idbagian = $this->input->post('idbagian');

		$query = $this->karyawan_model->updatekaryawan($namakaryawan, $alamat, $idbagian, $id_karyawan);

		header('location:'.base_url('index.php/c_karyawan').$this->index());
	}

	function anggota_bagian($idbagian){ 
		$output = '';
		$no = 0;
		$data_anggota = $this->karyawan_model->tampil_anggota($idbagian)->result_array();
		foreach ($data_anggota as $anggota) {
			$no++;
			$output .='
				<tr>
					<td>'.$anggota['id_karyawan'].'</td>
					<td>'.$anggota['nama_karyawan'].'</td>
					<td>'.$anggota['alamat'].'</td>
				</tr>
			';
		}
		return $output;
	}

	function getdata($idbagian) {
		echo $this->anggota_bagian($idbagian);
	}
	
}