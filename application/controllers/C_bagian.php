<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_bagian extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('bagian_model');
	}

	public function index(){
		$data['bagian'] = $this->bagian_model->getAllBagian();
		$this->load->view('bagian_list.php', $data);
	}

	public function insert(){
		$id_bagian = "";
		$nama_bagian = $this->input->post('nama_bagian');
		
		$this->bagian_model->insertbagian($id_bagian,$nama_bagian);

		header('location:'.base_url('index.php/c_bagian').$this->index());

	}

	public function delete($id_bagian,$nama_bagian){
		$query = $this->bagian_model->deletebagian($id_bagian, $nama_bagian);

			header('location:'.base_url('index.php/c_bagian').$this->index());
	}

	public function edit($id_bagian){
		$namabagian = $this->input->post('namabagian');

		$query = $this->bagian_model->updatebagian($namabagian, $id_bagian);

		header('location:'.base_url('index.php/c_bagian').$this->index());
	}

}