<?php
	class Karyawan_model extends CI_Model {
		function __construct(){
			parent::__construct();
			$this->load->database();
		}

		public function getAllKaryawan(){
			//$sql="select * from users";
			//$query = $this->db->query($sql);
			$query = $this->db->get('tblkaryawan');
			return $query->result(); 
		}

		public function insertkaryawan($namakaryawan,$alamat,$idbagian){
			$sql="INSERT INTO tblkaryawan VALUES('','$namakaryawan', '$alamat','$idbagian')";
			$this->db->query($sql);
		}

		public function deletekaryawan($id_karyawan){
			$this->db->where('tblkaryawan.id_karyawan', $id_karyawan);
			return $this->db->delete('tblkaryawan');
		}

		public function updatekaryawan($namakaryawan, $alamat, $idbagian,$id_karyawan){
			$this->db->query("UPDATE tblkaryawan SET nama_karyawan ='$namakaryawan', alamat ='$alamat', id_bagian ='$idbagian' WHERE id_karyawan = '$id_karyawan' ");
		}

		function tampil_anggota($idbagian) {
			$query = $this->db->get_where('tblkaryawan', array('id_bagian' => $idbagian));
			return $query;
		}
	}